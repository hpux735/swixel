#swixel

[![Version](https://img.shields.io/badge/version-v0.0.1-blue.svg)](https://github.com/hpux735/swixel/releases/latest)
![Platforms](https://img.shields.io/badge/platforms-linux-yellow.svg)
![Architectures](https://img.shields.io/badge/architectures-arm-green.svg)
![Languages](https://img.shields.io/badge/languages-swift-orange.svg)
![License](https://img.shields.io/badge/license-MIT-blue.svg)

**swixel** is a library for interfacing with **WS281x**-based LED lights.  The goal is to support Raspberry Pi2, Beaglebone, and more.

Work has just begun, so if you interested in helping, drop me a line.

## Protocol specification
**Adafruit** hosts the [datasheet](https://www.adafruit.com/datasheets/WS2811.pdf) for the device.  The best way to think of them is as a very long shift register that uses pulse-width modulation.  By that I mean that there is no data and clock pins.  One wire is used for communication.  There are two speeds that are selectable when the chip is used, and it's likely selected by the manufacturer of the lights that you're using.  I'm no expert on what's available, but the LEDs that I have use the high speed mode.

|               |   Low  speed  |  High speed  |
|--------------:|:-------------:|:------------:|
| 1 High period | 1200 ± 150 nS |  600 ± 75 nS |
| 0 High period |  500 ± 150 nS |  250 ± 75 nS |
|  1 Low period | 1300 ± 150 nS |  650 ± 75 nS |
|  0 Low period | 2000 ± 150 nS | 1000 ± 75 nS |
|         Total |       2500 nS |      1250 nS |
|         Reset |       > 50 μS |      > 25 μS |

During idle periods the pin should be low.  There are a number of edge cases that may be useful or important while driving this protocol from linux systems.  The reason for this is that relative a microcontroller it's much more difficult to precisely drive the protocol.

For example, the current implementation on the Beaglebone (ab)uses the SPI bus to generate the waveform.  On the beagleone the idle state for SPI data out pin is *high*, so it's not obvious how to make it work.  It would be possible to use a inverting level shifter, but that's not an avenue I've explored extensively yet.

Another consideration is that the WS2811 needs a *high* drive level 0.7*V<sub>DD</sub> > V<sub>IH</sub> > V<sub>DD</sub>+0.5.  Most (if not all) ARM devices drive the output pins at a maximum of 3.3 volts.  Therefore, some kind of level translator is necessary.  I've used a dual-MOSFET non-inverting solution in most of my tests.
![Schematic](https://raw.githubusercontent.com/hpux735/swixel/master/Documentation/Media/schematic-non-invert.png)
![Simulation](https://raw.githubusercontent.com/hpux735/swixel/master/Documentation/Media/simulator-non-inverting.png)
![Oscilloscope](https://raw.githubusercontent.com/hpux735/swixel/master/Documentation/Media/oscilloscope-non-inverting.JPG)

The non-inverting converter burns about 5mA on average. To get a reasonable output of an inverting version, at least 7-10mA is necessary at idle.

